import React from "react";
import PropTypes from "prop-types";

const Weather = ({ result }) => {
  // Extraer datos del response
  const { name, main } = result;

  // Evita el error al no haber datos aun
  if (!name) return null;

  // base para convertir temperatura
  const kelvin = 273.15;

  return (
    <div className="card-panel white col s12">
      <div className="black-text">
        <h2>El clima en {name} es: </h2>
        <p className="temperatura">
          {parseInt(main.temp - kelvin)} <span> &#x2103;</span>
        </p>
        <p>
          Máxima:
          {parseInt(main.temp_max - kelvin)} <span> &#x2103;</span>
        </p>
        <p>
          Mínima:
          {parseInt(main.temp_min - kelvin)} <span> &#x2103;</span>
        </p>
      </div>
    </div>
  );
};

Weather.propTypes = {
  result: PropTypes.object.isRequired,
};

export default Weather;
