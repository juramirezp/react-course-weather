import React from "react";
import PropTypes from "prop-types";

const Error = ({ mesage }) => {
  return <p className="red error">{mesage}</p>;
};

Error.propTypes = {
  mesage: PropTypes.string.isRequired,
};

export default Error;
