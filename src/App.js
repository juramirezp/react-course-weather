import React, { Fragment, useState, useEffect } from "react";
import Form from "./components/Form";
import Header from "./components/Header";
import Weather from "./components/Weather";
import Error from "./components/Error";

function App() {
  // Creacion del state
  const [search, setSearch] = useState({
    city: "",
    country: "",
  });
  const [consult, setConsult] = useState(false);
  const [result, setResult] = useState({});
  const [error, setError] = useState(false);

  const { city, country } = search;

  useEffect(() => {
    if (consult) {
      const APIconsult = async () => {
        const key = "a543c9c322ae29b154ceae184667dd00";
        const url = `https://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${key}`;

        const request = await fetch(url);
        const response = await request.json();

        setResult(response);
        setConsult(false);

        // Validar si hubo error en la cosnulta
        if (response.cod === "404") {
          setError(true);
        } else {
          setError(false);
        }
      };

      APIconsult();
      // eslint-disable-next-line
    }
  }, [consult, city, country]);

  let component;
  if (error) {
    component = <Error mesage="No existen resultados" />;
  } else {
    component = <Weather result={result}></Weather>;
  }

  return (
    <Fragment>
      <Header title="Weather App"></Header>

      <div className="contenedor-form">
        <div className="container">
          <div className="row">
            <div className="col m6 s12">
              <Form
                search={search}
                setSearch={setSearch}
                setConsult={setConsult}
              ></Form>
            </div>
            <div className="col m6 s12">{component}</div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default App;
